﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Print_Label
{
    class SQLConnecton
    {
        public SqlConnection con;
        public SqlConnection con1;
        public SqlDataAdapter ADP = new SqlDataAdapter();
        public SqlDataAdapter ADP1 = new SqlDataAdapter();


        public void getConnection()
        {
            try
            {
                con = new SqlConnection();
                con.Close();
                if (con.State == ConnectionState.Open)
                {
                    con.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con.ConnectionString = connectionString;
                con.Open();

            }
            catch (Exception ex)
            {
                string[] lines = new string[] { ex + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                //Console.Write(ex);
                con.Close();
            }

        }



        public void getConnection1()
        {
            try
            {
                con1 = new SqlConnection();
                con1.Close();
                if (con1.State == ConnectionState.Open)
                {
                    con1.Close();

                }
                string connectionString = ConfigurationManager.ConnectionStrings["ConStringMSSQL"].ConnectionString;
                con1.ConnectionString = connectionString;
                con1.Open();

            }
            catch (Exception ex)
            {
                //Console.Write(ex);
                string[] lines = new string[] { ex + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                con1.Close();
            }

        }




        public int Execute_Query1(string sql_query)
        {
            int result = 0;
            getConnection1();
            try
            {
                SqlCommand cmd1 = new SqlCommand(sql_query, con1);
                result = cmd1.ExecuteNonQuery();
                con1.Close();
            }
            catch (Exception ex)
            {
                //Console.Write(ex);
                string[] lines = new string[] { ex + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                con1.Close();
            }
            return result;
        }

        public DataTable DataRetrieval(string strcon)
        {
            getConnection();
            DataTable dt_Schedule = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand(strcon, con);
                ADP.SelectCommand = cmd;
                ADP.Fill(dt_Schedule);
                con.Close();
            }
            catch (Exception ex)
            {
                // Console.Write(ex);
                string[] lines = new string[] { ex + DateTime.Now.ToString() };
                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                con.Close();
            }
            return dt_Schedule;
        }

        public DataTable OpenPendingPrintData()
        {
            DataTable dt_Data = new DataTable();
            string ls_sql = "select * from Trace_Tag_Generation where Print_Status=0 order by Printed_DateTime asc";
            dt_Data = DataRetrieval(ls_sql);
            return dt_Data;
        }
        public int UpdatePrintingStatus(string TagId)
        {

            string ls_sql = "update Trace_Tag_Generation set Print_Status = 1 where TagId = '"+ TagId + "' ";
            return Execute_Query1(ls_sql);
        }



    }

}

