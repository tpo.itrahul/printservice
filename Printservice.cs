﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using QRCoder;

namespace Print_Label
{

    public class Printservice
    {

        private readonly Timer _timer;

        static DataTable Print_Label = new DataTable();

        SQLConnecton sg = new SQLConnecton();
        public string No_of_pages { get; set; }
        public string PrintData { get; set; }
        public string PrintDatahold { get; set; }
        public string PrintDataok { get; set; }
    public Printservice()
        {

            _timer = new Timer(5000) { AutoReset = true };
            _timer.Elapsed += timerElapsed;

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            string[] lines = new string[] {"Printer Service Started "+ DateTime.Now.ToString() };
            File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
            OpenPendingPrintData();
        }
        private void OpenPendingPrintData()
        {
            try
            {
                DataTable PrintList = sg.OpenPendingPrintData();
                PrintDocument pd1 = new PrintDocument();
                pd1.PrintController = new StandardPrintController();
                foreach (DataRow row in PrintList.Rows)
                {
                    var TagId = row[0].ToString();
                    var Printer_Name = row[2].ToString();
                    //var Printer_Ip = row[3].ToString();
                    var Module_Name = row[5].ToString().TrimEnd();
                   //Console.WriteLine(Module_Name);
                    var No_Of_Copies = row[10].ToString();
                    switch (Module_Name)
                    {
                        case "FGSFIFO":
                            {

                                string connectionString = ConfigurationManager.ConnectionStrings["PrinterFGS"].ConnectionString;
                                if (connectionString == "")
                                {
                                    string[] BREAK = new string[] { "Please check connectionString of PrinterFGS: " + DateTime.Now.ToString("dd MMM yyyy") };
                                    File.AppendAllLines(@"C:\Demo\PrintService.txt", BREAK);
                                    break;

                                }
                                // Console.WriteLine(connectionString);
                                // Console.WriteLine("FGS Print Invoked" + DateTime.Now.ToString("dd MMM yyyy"));
                                string[] lines = new string[] { "FGS Print Invoked on Printer"+ connectionString + DateTime.Now.ToString("dd MMM yyyy") };
                                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                                PrintData = row[9].ToString();
                                pd1.PrintPage += PrintDocument1_PrintFGS;
                                pd1.PrinterSettings.PrinterName = connectionString;
                               // Console.WriteLine("Initiated Tag Printing from FGS to " + pd1.PrinterSettings.PrinterName + " " + DateTime.Now.ToString());
                                pd1.PrinterSettings.Copies = Convert.ToInt16(No_Of_Copies);
                                sg.UpdatePrintingStatus(row[0].ToString());
                               //Console.WriteLine("Came out updation");
                               //Console.WriteLine("Landscape assigned");
                                pd1.DefaultPageSettings.Landscape = false;
                               //Console.WriteLine("Printing started");
                                pd1.Print();
                                //Console.WriteLine("Printing completed");
                                PrintData = "";

                                break;
                            }
                        case "DUALHOLD":
                            {
                                if (Printer_Name == "Dual1")
                                {
                                    string connectionString = ConfigurationManager.ConnectionStrings["PrinterDual1"].ConnectionString;
                                    if (connectionString  == "")
                                    {
                                        string[] BREAK = new string[] { "Please check connectionString of PrinterDual1: " + DateTime.Now.ToString("dd MMM yyyy") };
                                        File.AppendAllLines(@"C:\Demo\PrintService.txt", BREAK);
                                        break;
                                    }
                                    //Console.WriteLine("DUALHOLD Print Invoked" + DateTime.Now.ToString("dd MMM yyyy"));
                                    string[] lines = new string[] { "DUALHOLD Print Invoked on Printer" + connectionString + DateTime.Now.ToString("dd MMM yyyy") };
                                    File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                                    PrintDatahold = row[9].ToString();
                                    pd1.PrintPage += PrintDocument1_PrintDUALHOLD;
                                    pd1.PrinterSettings.PrinterName = connectionString;
                                    //Console.WriteLine("Initiated Tag Printing from DUALHOLD to " + pd1.PrinterSettings.PrinterName + " " + DateTime.Now.ToString());
                                    pd1.PrinterSettings.Copies = 1;
                                    sg.UpdatePrintingStatus(row[0].ToString());
                                    //Console.WriteLine("Came out updation");
                                    //Console.WriteLine("Landscape assigned");
                                    pd1.DefaultPageSettings.Landscape = false;
                                    //Console.WriteLine("Printing started");
                                    pd1.Print();
                                    //Console.WriteLine("Printing completed");
                                    PrintDatahold = "";
                                }
                                else if (Printer_Name == "Dual2")
                                {
                                    string connectionString = ConfigurationManager.ConnectionStrings["PrinterDual2"].ConnectionString;
                                    if (connectionString == "")
                                    {
                                        string[] BREAK = new string[] { "Please check connectionString of PrinterDual2: " + DateTime.Now.ToString("dd MMM yyyy") };
                                        File.AppendAllLines(@"C:\Demo\PrintService.txt", BREAK);
                                        break;
                                    }
                                    //Console.WriteLine("DUALHOLD Print Invoked" + DateTime.Now.ToString("dd MMM yyyy"));
                                    string[] lines = new string[] { "DUALHOLD Print Invoked on Printer" + connectionString + DateTime.Now.ToString("dd MMM yyyy") };
                                    File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                                    PrintDatahold = row[9].ToString();
                                    pd1.PrintPage += PrintDocument1_PrintDUALHOLD;
                                    pd1.PrinterSettings.PrinterName = connectionString;
                                    //Console.WriteLine("Initiated Tag Printing from DUALHOLD to " + pd1.PrinterSettings.PrinterName + " " + DateTime.Now.ToString());
                                    pd1.PrinterSettings.Copies = 1;
                                    sg.UpdatePrintingStatus(row[0].ToString());
                                    //Console.WriteLine("Came out updation");
                                    //Console.WriteLine("Landscape assigned");
                                    pd1.DefaultPageSettings.Landscape = false;
                                    //Console.WriteLine("Printing started");
                                    pd1.Print();
                                    //Console.WriteLine("Printing completed");
                                    PrintDatahold = "";
                                }

                                break;
                            }
                        case "DUALOK":
                            {
                                if (Printer_Name == "Dual1")
                                {
                                    string connectionString = ConfigurationManager.ConnectionStrings["PrinterDual1"].ConnectionString;
                                    if (connectionString == "")
                                    {
                                        string[] BREAK = new string[] { "Please check connectionString of PrinterDual1: " + DateTime.Now.ToString("dd MMM yyyy") };
                                        File.AppendAllLines(@"C:\Demo\PrintService.txt", BREAK);
                                        break;
                                    }
                                    //Console.WriteLine("DUALOK Print Invoked" + DateTime.Now.ToString("dd MMM yyyy"));
                                    string[] lines = new string[] { "DUALOK Print Invoked on Printer" + connectionString + DateTime.Now.ToString("dd MMM yyyy") };
                                    File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                                    PrintDataok = row[9].ToString();
                                    pd1.PrintPage += PrintDocument1_PrintDUALOK;
                                    pd1.PrinterSettings.PrinterName = connectionString;
                                    //Console.WriteLine("Initiated Tag Printing from DUALOK to " + pd1.PrinterSettings.PrinterName + " " + DateTime.Now.ToString());
                                    pd1.PrinterSettings.Copies = Convert.ToInt16(No_Of_Copies);
                                    sg.UpdatePrintingStatus(row[0].ToString());
                                    //Console.WriteLine("Came out updation");
                                    //Console.WriteLine("Landscape assigned");
                                    pd1.DefaultPageSettings.Landscape = false;
                                    //Console.WriteLine("Printing started");
                                    pd1.Print();
                                    //Console.WriteLine("Printing completed");
                                    PrintDataok = "";
                                }
                                else if(Printer_Name == "Dual2")
                                {
                                    string connectionString = ConfigurationManager.ConnectionStrings["PrinterDual2"].ConnectionString;
                                    if (connectionString == "")
                                    {
                                        string[] BREAK = new string[] { "Please check connectionString of PrinterDual2: " + DateTime.Now.ToString("dd MMM yyyy") };
                                        File.AppendAllLines(@"C:\Demo\PrintService.txt", BREAK);
                                        break;
                                    }
                                    //Console.WriteLine("DUALOK Print Invoked" + DateTime.Now.ToString("dd MMM yyyy"));
                                    string[] lines = new string[] { "DUALOK Print Invoked on Printer" + connectionString + DateTime.Now.ToString("dd MMM yyyy") };
                                    File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
                                    PrintDataok = row[9].ToString();
                                    pd1.PrintPage += PrintDocument1_PrintDUALOK;
                                    pd1.PrinterSettings.PrinterName = connectionString;
                                    //Console.WriteLine("Initiated Tag Printing from DUALOK to " + pd1.PrinterSettings.PrinterName + " " + DateTime.Now.ToString());
                                    pd1.PrinterSettings.Copies = Convert.ToInt16(No_Of_Copies);
                                    sg.UpdatePrintingStatus(row[0].ToString());
                                    //Console.WriteLine("Came out updation");
                                    //Console.WriteLine("Landscape assigned");
                                    pd1.DefaultPageSettings.Landscape = false;
                                    //Console.WriteLine("Printing started");
                                    pd1.Print();
                                    //Console.WriteLine("Printing completed");
                                    PrintDataok = "";
                                }
                                break;
                            }
                    }
                }
            }
            catch(Exception ex)
            {
                //Console.WriteLine(ex);
                string[] lines = new string[] { "OpenPendingPrintData :" + ex + DateTime.Now.ToString("dd MMM yyyy") };
                File.AppendAllLines(@"C:\Demo\PrintService.txt", lines);
            }

        }

        private void PrintDocument1_PrintFGS(System.Object sender, System.Drawing.Printing.PrintPageEventArgs e) // Handles PrintDocument1.PrintPage
        {

            string[] arr = PrintData.Split('|');
            string[] Barcode = arr[0].Split(':');
            string[] Supplier = arr[2].Split(':');
            string[] Material = arr[4].Split(':');
            string[] Batch = arr[6].Split(':');
            string[] Quantity = arr[8].Split(':');
            string[] InvoiceNo = arr[10].Split(':');
            string[] InvoiceDate = arr[12].Split(':');
            string[] Location = arr[14].Split(':');
            string[] Month = arr[16].Split(':');
           Console.WriteLine(Barcode[1]+" "+ Supplier[1] + " " + Material[1] + " "+ Batch[1] + " "+Quantity[1] + " "+InvoiceNo[1] + " "+InvoiceDate[1] + " "+Location[1]+" "+Month[1]);


            Font printFont = new Font("Bell MT Bold", 12.0F, FontStyle.Bold);
            Font printFontSmall = new Font("Bell MT Bold", 8.0F, FontStyle.Bold);
            Font printFontHead = new Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold);
            Font printFontHeadHigh = new Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold);
            Font printFontHeadHighest = new Font("Bell MT Bold", 36.0F, FontStyle.Bold);

            e.Graphics.DrawString(Barcode[1], printFontSmall, Brushes.Black, 200, 20);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(Barcode[1], QRCodeGenerator.ECCLevel.H);
            QRCode qrCode = new QRCode(qrCodeData);


             Bitmap bitmap = qrCode.GetGraphic(2);
            MemoryStream ms = new MemoryStream();
             bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

            Pen blackPen = new Pen(Color.Black, 3);

             e.Graphics.DrawString("FGS", printFontHeadHighest, Brushes.Black, 30, 10);

            e.Graphics.DrawImage(bitmap, 200, 40);
            e.Graphics.DrawString("Material :", printFont, Brushes.Black, 40, 80);

            e.Graphics.DrawString(Material[1], printFont, Brushes.Black, 40, 110);

            e.Graphics.DrawString("Quantity :", printFont, Brushes.Black, 40, 140);


            e.Graphics.DrawString(Quantity[1], printFont, Brushes.Black, 130, 140);

            e.Graphics.DrawString("Invoice No :", printFont, Brushes.Black, 40, 170);

            e.Graphics.DrawString(InvoiceNo[1], printFont, Brushes.Black, 190, 170);

            e.Graphics.DrawString("Invoice Date :", printFont, Brushes.Black, 40, 200);

            e.Graphics.DrawString(InvoiceDate[1], printFont, Brushes.Black, 190, 200);

            e.Graphics.DrawString("Supplier :", printFont, Brushes.Black, 40, 230);

            e.Graphics.DrawString(Supplier[1], printFont, Brushes.Black, 130, 230);




            e.Graphics.DrawString("Location :", printFont, Brushes.Black, 40, 260);

            e.Graphics.DrawString(Location[1], printFont, Brushes.Black, 130, 260);



            e.Graphics.DrawString("Batch :", printFont, Brushes.Black, 200, 260);

            e.Graphics.DrawString(Batch[1], printFont, Brushes.Black, 280, 260);
        }
        /// <summary>
        /// DUAL HOLD
        /// </summary>
        ///
        private void PrintDocument1_PrintDUALHOLD(System.Object sender, System.Drawing.Printing.PrintPageEventArgs e) // Handles PrintDocument1.PrintPage
        {

            string[] arr = PrintDatahold.Split('|');
            string[] Barcode = arr[0].Split(':');
            string[] Code = arr[2].Split(':');
            string[] Tread = arr[4].Split(':');
            string[] Dual = arr[6].Split(':');
            string[] Nos = arr[8].Split(':');
            string[] Operators = arr[10].Split(':');
            string[] LTNo = arr[12].Split(':');
            string[] TruckNo = arr[14].Split(':');
            string[] ProDate = arr[16].Split(' ');
            string[] ShiftId = arr[18].Split(':');
            string[] PrintTime = arr[22].Split(' ');
            string[] HoldRsn = arr[24].Split('-');

            // Console.WriteLine(Barcode[1]+" "+ Supplier[1] + " " + Material[1] + " "+ Batch[1] + " "+Quantity[1] + " "+InvoiceNo[1] + " "+InvoiceDate[1] + " "+Location[1]+" "+Month[1]);

            Font printFont = new Font("Bell MT Bold", 12.0F, FontStyle.Bold);
            Font printFontSmall = new Font("Bell MT Bold", 8.0F, FontStyle.Bold);
            Font printFontHead = new Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold);
            Font printFontHeadHigh = new Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold);
            Font printFontHeadHighest = new Font("Bell MT Bold", 36.0F, FontStyle.Bold);

            e.Graphics.DrawString(Barcode[1], printFontSmall, Brushes.Black, 190, 20);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(Barcode[1], QRCodeGenerator.ECCLevel.H);
            QRCode qrCode = new QRCode(qrCodeData);


            Bitmap bitmap = qrCode.GetGraphic(2);
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

            Pen blackPen = new Pen(Color.Black, 3);

            int x = 2;
            int y = 130;
            int width = 280;
            int height = 40;

            //Draw faid Hold diagonally
            e.Graphics.RotateTransform(50); // rotate 45° clockwise
            Font myfont = new Font("Arial", 80);
            SolidBrush myBrush = new SolidBrush(Color.FromArgb(30, 0, 0, 0));
            e.Graphics.DrawString("HOLD", myfont, myBrush, 110, 0); // this location is relative to the point of rotation
            e.Graphics.ResetTransform();



            // Draw rectangle to screen.
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500);
            e.Graphics.DrawString("HOLDTREAD", printFontHeadHigh, Brushes.Black, 1, 90);

            e.Graphics.DrawImage(bitmap, 190, 40);



            e.Graphics.DrawString("Code", printFont, Brushes.Black, 20, 140);

            e.Graphics.DrawString(Code[1], printFont, Brushes.Black, 130, 140);

            x = 2;
            y = 170;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("Tread", printFont, Brushes.Black, 20, 180);

            e.Graphics.DrawString(Tread[1], printFont, Brushes.Black, 130, 180);
            x = 2;
            y = 210;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220);


            e.Graphics.DrawString(Nos[1], printFont, Brushes.Black, 130, 220);
            e.Graphics.DrawString(Dual[1], printFont, Brushes.Black, 210, 220);

            x = 2;
            y = 250;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);

            e.Graphics.DrawString("Date/Shift", printFont, Brushes.Black, 20, 260);

            e.Graphics.DrawString(ProDate[2] + "-- " + ShiftId[1], printFont, Brushes.Black, 130, 260);



            x = 2;
            y = 290;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);

            e.Graphics.DrawString("Time", printFont, Brushes.Black, 20, 300);
            e.Graphics.DrawString(PrintTime[4], printFont, Brushes.Black, 130, 300);

            x = 1;
            y = 330;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            e.Graphics.DrawString("Location", printFont, Brushes.Black, 20, 340);
            // e.Graphics.DrawString(location[1], printFont, Brushes.Black, 130, 340)


            x = 2;
            y = 370;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("Operators", printFont, Brushes.Black, 20, 380);
            e.Graphics.DrawString(Operators[1], printFont, Brushes.Black, 130, 380);



            e.Graphics.DrawString("LTNo :", printFont, Brushes.Black, 20, 420);
            e.Graphics.DrawString("TruckNo:", printFont, Brushes.Black, 140, 420);

            e.Graphics.DrawString(LTNo[1], printFont, Brushes.Black, 100, 420);
            e.Graphics.DrawString(TruckNo[1], printFont, Brushes.Black, 220, 420);

            x = 2;
            y = 410;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);

            e.Graphics.DrawString("HRsn:", printFont, Brushes.Black, 20, 460);
            e.Graphics.DrawString(HoldRsn[1], printFont, Brushes.Black, 70, 460);

            x = 2;
            y = 450;

            e.Graphics.DrawRectangle(blackPen, x, y, width, height);




            Point Line1 = new Point(130, 130);
            Point Line2 = new Point(130, 410);

            // Draw line to screen.
            e.Graphics.DrawLine(blackPen, Line1, Line2);

            Point Line3 = new Point(210, 210);
            Point Line4 = new Point(210, 250);

            // Draw line to screen.
           e.Graphics.DrawLine(blackPen, Line3, Line4);

            //Cross Line
            Point Line5 = new Point(0, 0);
            Point Line6 = new Point(300, 500);

            // Draw line to screen.
            e.Graphics.DrawLine(blackPen, Line5, Line6);

            Point Line7 = new Point(0, 500);
            Point Line8 = new Point(300, 0);

            // Draw line to screen.
            e.Graphics.DrawLine(blackPen, Line7, Line8);

        }


        /// <summary>
        /// DUALOK
        /// </summary>
        ///
        private void PrintDocument1_PrintDUALOK(System.Object sender, System.Drawing.Printing.PrintPageEventArgs e) // Handles PrintDocument1.PrintPage
        {

            string[] arr = PrintDataok.Split('|');
            string[] Barcode = arr[0].Split(':');
            string[] Code = arr[2].Split(':');
            string[] Tread = arr[4].Split(':');
            string[] Dual = arr[6].Split(':');
            string[] Nos = arr[8].Split(':');
            string[] Operators = arr[10].Split(':');
            string[] LTNo = arr[12].Split(':');
            string[] TruckNo = arr[14].Split(':');
            string[] ProDate = arr[16].Split(' ');
            string[] PrintTime = arr[16].Split(' ');
            string[] ShiftId = arr[18].Split(':');
            string[] CompoundCode = arr[20].Split(':');


            Font printFont = new Font("Bell MT Bold", 12.0F, FontStyle.Bold);
            Font printFontSmall = new Font("Bell MT Bold", 8.0F, FontStyle.Bold);
            Font printFontHead = new Font(FontFamily.GenericMonospace, 18.0F, FontStyle.Bold);
            Font printFontHeadHigh = new Font(FontFamily.GenericMonospace, 26.0F, FontStyle.Bold);
            Font printFontHeadHighest = new Font("Bell MT Bold", 36.0F, FontStyle.Bold);

            e.Graphics.DrawString(Barcode[1], printFontSmall, Brushes.Black, 190, 20);

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(Barcode[1], QRCodeGenerator.ECCLevel.H);
            QRCode qrCode = new QRCode(qrCodeData);


            Bitmap bitmap = qrCode.GetGraphic(2);
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

            Pen blackPen = new Pen(Color.Black, 3);

            int x = 2;
            int y = 130;
            int width = 280;
            int height = 40;
            // Draw rectangle to screen.
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            e.Graphics.DrawString("FT-01-B5:02", printFontSmall, Brushes.Black, 20, 500);
            e.Graphics.DrawString("TREAD", printFontHeadHigh, Brushes.Black, 5, 80);

            e.Graphics.DrawImage(bitmap, 190, 40);



            e.Graphics.DrawString("Code", printFont, Brushes.Black, 20, 140);

            e.Graphics.DrawString(Code[1], printFont, Brushes.Black, 130, 140);

            x = 2;
            y = 170;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("Tread", printFont, Brushes.Black, 20, 180);

            e.Graphics.DrawString(Tread[1], printFont, Brushes.Black, 130, 180);
            x = 2;
            y = 210;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("NOS", printFont, Brushes.Black, 20, 220);


            e.Graphics.DrawString(Nos[1], printFont, Brushes.Black, 130, 220);
            e.Graphics.DrawString(Dual[1], printFont, Brushes.Black, 210, 220);

            x = 2;
            y = 250;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);

            e.Graphics.DrawString("Date/Shift", printFont, Brushes.Black, 20, 260);

            e.Graphics.DrawString(ProDate[2] + "--" + ShiftId[1], printFont, Brushes.Black, 130, 260);



            x = 2;
            y = 290;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);

            e.Graphics.DrawString("Time", printFont, Brushes.Black, 20, 300);
            e.Graphics.DrawString(PrintTime[3], printFont, Brushes.Black, 130, 300);

            x = 1;
            y = 330;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            e.Graphics.DrawString("Location", printFont, Brushes.Black, 20, 340);
            // e.Graphics.DrawString(location[1], printFont, Brushes.Black, 130, 340)


            x = 2;
            y = 370;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);


            e.Graphics.DrawString("Operators", printFont, Brushes.Black, 20, 380);
            e.Graphics.DrawString(Operators[1], printFont, Brushes.Black, 130, 380);



            e.Graphics.DrawString("LTNo :", printFont, Brushes.Black, 20, 420);
            e.Graphics.DrawString("TruckNo:", printFont, Brushes.Black, 140, 420);

            e.Graphics.DrawString(LTNo[1], printFont, Brushes.Black, 100, 420);
            e.Graphics.DrawString(TruckNo[1], printFont, Brushes.Black, 220, 420);

            x = 2;
            y = 410;
            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            e.Graphics.DrawString("Comp:", printFont, Brushes.Black, 20, 460);
            e.Graphics.DrawString(CompoundCode[1], printFont, Brushes.Black, 70, 460);

            x = 2;
            y = 450;

            e.Graphics.DrawRectangle(blackPen, x, y, width, height);
            Point Line1 = new Point(130, 130);
            Point Line2 = new Point(130, 410);

            // Draw line to screen.
            e.Graphics.DrawLine(blackPen, Line1, Line2);

            Point Line3 = new Point(210, 210);
            Point Line4 = new Point(210, 250);

            // Draw line to screen.
            e.Graphics.DrawLine(blackPen, Line3, Line4);

        }




        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}
